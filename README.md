# Docker Templates

This repository contains the docker-compose files to recreate the architecture used in the Addmin project.

The architecture is composed of:
- **Apache Flink**: an unified stream-processing and batch-processing framework
- **Apache Kafka**: a distributed event store and stream-processing platform
- **RabbitMQ**: a message-oriented middleware

---

### How-To

Before running the services you have to create the network they are all using. You can create it by running the following command:

`docker network create flink-kafka`

where `flink-kafka` is the name of the network that is defined in every `docker-compose.yml` file.

Once you have created the common network you can run the services using the usual `docker compose up` command (or `docker-compose up` if you are not using the latest version of Docker Compose).

Please check each service directory for custom configuration.

---

To know more about Docker and Docker Compose check the official documentation at https://docs.docker.com/get-started/ and https://docs.docker.com/compose/.