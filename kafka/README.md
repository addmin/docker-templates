# Apache Kafka - Docker

The `kafka` service needs an environment variable, called `HOSTNAME` to be set.

You can rename the `.env.sample` to `.env` file and run the following command

```
docker-compose --env-file .env up
```